Ibotta Dev Project
=========


# The Solution

---

## Completed Items 

- `POST /words.json`: Takes a JSON array of English-language words and adds them to the corpus (data store).
- `GET /anagrams/:word.json`:
  - Returns a JSON array of English-language words that are anagrams of the word passed in the URL.
  - This endpoint should support an optional query param that indicates the maximum number of results to return.
- `DELETE /words/:word.json`: Deletes a single word from the data store.
- `DELETE /words.json`: Deletes all contents of the data store.
- Endpoint that identifies words with the most anagrams

## Running the application

The final executable is at target/demo-0.0.1-SNAPSHOT.jar.
To execute it from the command line, use `java -jar demo-0.0.1-SNAPSHOT.jar`
It will bind to port 3000 as expected.

## Building the application

Should you need to, you can build the application from the repository root with `mvn package`.

## Tests

There are numerous unit tests in the code base, but also one was added to the ruby test suite to test the most anagrams endpoint.
These tests are located in `platform_dev/anagram_test.rb`

I am a fan of tests as documentation. Hopefully, you will find the unit tests to be expressive for the components in addition to the
ruby integration tests.

## Final thoughts

Thank you, I enjoyed this exercise and had to exercise restraint to not spend much more time playing with it. In the end,
I couldn't leave it without finding out what actually was the largest set of anagrams in the dictionary. :)

Regards,

Joe Gee
joe@rmprogrammers.com