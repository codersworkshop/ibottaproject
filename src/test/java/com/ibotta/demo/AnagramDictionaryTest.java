package com.ibotta.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class AnagramDictionaryTest {
	private AnagramDictionary dictionary = new AnagramDictionary();

	@Test
	public void emptyDictionary() {
		Assert.assertTrue(dictionary.listAnagrams("bob").isEmpty());
	}

	@Test
	public void emptyString() {
		dictionary.add(createSet(""));
		Assert.assertEquals(0, dictionary.size());
	}

	@Test
	public void skipSelf() {
		dictionary.add(createSet("dear"));
		Assert.assertTrue(dictionary.listAnagrams("dear").isEmpty());
	}

	@Test
	public void multipleAdd() {
		dictionary.add(createSet("dear"));
		dictionary.add(createSet("dear"));
		Assert.assertEquals(1, dictionary.listAnagrams("read").size());
	}

	@Test
	public void findAnagrams() {
		dictionary.add(createSet("read", "dear", "dare"));
		Assert.assertEquals(createSet("dear", "dare"), dictionary.listAnagrams("read"));
	}

	@Test
	public void limitAnagrams() {
		dictionary.add(createSet("read", "dear", "dare"));
		Set<String> anagrams = dictionary.listAnagrams("read", 1);
		Assert.assertEquals("Wrong size result set", 1, anagrams.size());
		Assert.assertTrue("Wrong result returned", anagrams.contains("dear") || anagrams.contains("dare"));
	}

	@Test
	public void deleteSingleWord() {
		dictionary.add(createSet("read", "dear", "dare"));
		dictionary.delete("dear");
		Set<String> anagrams = dictionary.listAnagrams("read");
		Assert.assertFalse("Deleted word is still returned", anagrams.contains("dear"));
	}

	@Test
	public void caseInsensitiveAdd() {
		dictionary.add(createSet("DEAR"));
		Set<String> anagrams = dictionary.listAnagrams("read");

		Assert.assertEquals(createSet("dear"), anagrams);
	}

	@Test
	public void caseInsensitiveLookup() {
		dictionary.add(createSet("dear"));
		Set<String> anagrams = dictionary.listAnagrams("READ");

		Assert.assertEquals(createSet("dear"), anagrams);
	}

	@Test
	public void volumeTest() throws Exception {
		loadDictionaryFromFile();

		Set<String> expectedAnagrams = createSet("ared", "daer", "dare", "dear");
		Assert.assertEquals(expectedAnagrams, dictionary.listAnagrams("read"));
	}

	@Test
	public void mostAnagrams() {
		dictionary.add(createSet("bob", "obb"));
		dictionary.add(createSet("jim", "mij"));

		Set<String> largest = createSet("dare", "read", "dear");
		dictionary.add(largest);

		Set<String> set = dictionary.largestAnagramSet();
		Assert.assertEquals(new HashSet<String>(largest), set);
	}

	@Test
	public void mostDictionaryAnagrams() throws Exception {
		loadDictionaryFromFile();

		Set<String> largest = createSet("pales", "lapse", "spale", "lepas", "saple", "slape", "speal", "salep", "elaps",
				"sepal");
		Assert.assertEquals(largest, dictionary.largestAnagramSet());
	}

	private void loadDictionaryFromFile() throws IOException {
		try (InputStream in = getClass().getResourceAsStream("/dictionary.txt");
				BufferedReader dictionaryReader = new BufferedReader(new InputStreamReader(in))) {
			loadDictionaryFromStream(dictionaryReader);
		}
	}

	private void loadDictionaryFromStream(BufferedReader dictionaryReader) throws IOException {
		for (String word = dictionaryReader.readLine(); //
				word != null; //
				word = dictionaryReader.readLine()) {
			dictionary.add(Collections.singleton(word));
		}
	}

	private Set<String> createSet(String... strings) {
		Set<String> set = new HashSet<>();
		set.addAll(Arrays.asList(strings));
		return set;
	}
}
