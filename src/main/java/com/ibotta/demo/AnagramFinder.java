package com.ibotta.demo;

import java.util.Arrays;
import java.util.function.Predicate;

class AnagramFinder implements Predicate<String> {
	private String sortedWord;
	private String baseWord;

	public AnagramFinder(String baseWord) {
		this.baseWord = baseWord;
		sortedWord = sortedString(baseWord.toLowerCase());
	}

	public static String sortedString(String word) {
		char[] chars = word.toCharArray();
		Arrays.sort(chars);
		return new String(chars);
	}

	@Override
	public boolean test(String candidate) {
		if (baseWord.equals(candidate))
			return false;

		String sortedCandidate = sortedString(candidate);
		return sortedWord.equals(sortedCandidate);
	}
}