package com.ibotta.demo;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class AnagramDictionary {
	public static final long UNLIMITED = Long.MAX_VALUE;

	private Set<String> dictionary = ConcurrentHashMap.newKeySet();

	public void add(Set<String> newWords) {
		Set<String> lowercaseWords = newWords.stream() //
				.map(String::toLowerCase) //
				.filter(word -> !word.isEmpty()) //
				.collect(toSet());

		dictionary.addAll(lowercaseWords);
	}

	public Set<String> listAnagrams(String baseWord) {
		return listAnagrams(baseWord, UNLIMITED);
	}

	public Set<String> listAnagrams(String baseWord, long limit) {
		AnagramFinder finder = new AnagramFinder(baseWord);
		return dictionary.stream() //
				.filter(finder) //
				.limit(limit) //
				.collect(toSet());
	}

	public int size() {
		return dictionary.size();
	}

	public void delete(String word) {
		dictionary.remove(word);
	}

	public Set<String> largestAnagramSet() {
		Map<String, Long> counts = dictionary.stream() //
				.map(AnagramFinder::sortedString) //
				.collect(groupingBy(Function.identity(), counting()));

		String sortedString = counts.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();

		Set<String> largestSet = listAnagrams(sortedString);

		// if the sorted string happens to be a dictionary word,
		// it won't come back in the anagrams, so we have to add it
		if (dictionary.contains(sortedString))
			largestSet.add(sortedString);

		return largestSet;
	}

}
