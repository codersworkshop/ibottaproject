package com.ibotta.demo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AddWordRequest {
	private List<String> words;

	public List<String> getWords() {
		return words;
	}

	public Set<String> getDistinctWords() {
		return new HashSet<>(words);
	}

	public void setWords(List<String> words) {
		this.words = words;
	}
}
