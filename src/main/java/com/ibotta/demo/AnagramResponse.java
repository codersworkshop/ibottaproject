package com.ibotta.demo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AnagramResponse {
	private List<String> anagrams = new ArrayList<>();

	public AnagramResponse() {
	}

	public AnagramResponse(Collection<String> anagrams) {
		this.anagrams.addAll(anagrams);
	}

	public List<String> getAnagrams() {
		return anagrams;
	}

	public void setAnagrams(List<String> anagrams) {
		this.anagrams = anagrams;
	}
}
