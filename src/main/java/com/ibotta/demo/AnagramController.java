package com.ibotta.demo;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnagramController {
	private final Logger logger = LoggerFactory.getLogger(AnagramController.class);
	private AnagramDictionary dictionary = new AnagramDictionary();

	/*
	 * POST /words.json Takes a JSON array of English-language words and adds them
	 * to the corpus (data store).*
	 */
	@PostMapping("/words.json")
	@ResponseBody
	public ResponseEntity<?> addWords(@RequestBody AddWordRequest request) {
		logger.info("Add Words to Dictionary " + request.getWords().toString());
		dictionary.add(request.getDistinctWords());
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/*
	 * GET /anagrams/{word}.json: Returns a JSON array of English-language words
	 * that are anagrams of the word passed in the URL.
	 * 
	 * This endpoint supports an optional query param that indicates the maximum
	 * number of results to return.
	 */
	@GetMapping("/anagrams/{word}.json")
	public AnagramResponse getAnagrams(@PathVariable String word,
			@RequestParam(name = "limit", required = false) Long limit) {
		logger.info("Read anagrams for " + word + " limit " + limit);

		if (limit == null)
			limit = AnagramDictionary.UNLIMITED;

		Set<String> anagrams = dictionary.listAnagrams(word, limit);
		return new AnagramResponse(anagrams);
	}

	/*
	 * DELETE /words.json: Deletes all contents of the data store.
	 */
	@DeleteMapping("/words.json")
	@ResponseBody
	public ResponseEntity<?> deleteDictionary() {
		dictionary = new AnagramDictionary();
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/*
	 * DELETE /words/:word.json: Deletes a single word from the data store
	 */
	@DeleteMapping("/words/{word}.json")
	@ResponseBody
	public ResponseEntity<?> deleteWord(@PathVariable String word) {
		dictionary.delete(word);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/*
	 * Endpoint that identifies words with the most anagrams GET /anagrams '
	 */
	@GetMapping("/anagrams")
	@ResponseBody
	public ResponseEntity<AnagramResponse> searchCriteria(@RequestParam(name = "type") String type) {
		if (!"largest".equalsIgnoreCase(type))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		AnagramResponse response = new AnagramResponse(dictionary.largestAnagramSet());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
